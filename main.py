import pandas as pd
import csv

lines_csv = []
ship_type = []
rail_type = []
break_bulk_type = []

todo = 'todo.csv'
ship_csv = 'ship.csv'
rail_csv = 'rail.csv'
break_bulk_csv = 'breakbulk.csv'

projectSF = 'projectSF.csv'
projectSF_csv = []

projectPE = 'projectPE.csv'
projectPE_csv = []

projectJP = 'projectJP.csv'
projectJP_csv = []

projectJM = 'projectJM.csv'
projectJM_csv = []

huella_de_carbono_2022 = 'huella_de_carbono_2022.csv'
huella_de_carbono_2022_csv = []

HUELLA_2022 = 'HUELLA_2022.csv'
HUELLA_2022_csv = []

url = '/home/hugo/Descargas/'


class ProjectJP:
    project_jp = pd.read_excel(url + 'project_JP.xlsx', engine='openpyxl')
    lines = list(project_jp.T.to_dict().values())
    id_cargo_type = str
    tag = str

    for line in lines:

        tag = 'client:' + line['Owr'] + '|code:' + line['Code']

        if line['Type'] == 'Container':
            id_cargo_type = 'teu'
        else:
            id_cargo_type = 'conventional'

        lineCSV = {
            'PORT_ORIGIN': line['POL'],
            'PORT_DESTINATION': line['POD'],
            'STAGE_DISTANCE': "",
            'VEHICLES_NUMBER': "1",
            'ID_CARGO_TYPE': id_cargo_type,
            'TAGS': tag,
            'MATERIAL_NAME': "",
            'CARGO_TEU': line['uds de transporte'],
            'CARGO_WEIGHT': line['Mt/ud'],
            'GROSS_TONNAGE': "",
            'TOTAL_LOAD': line['Total MT'],
            'ID_SHIP_LOAD_TYPE': line['Type'],
            'ID_SHIP_TYPE': "",
            'PORT_OPERATIONS': "",
            'SHIP_FUEL_TYPE': ""
        }
        lines_csv.append(lineCSV)
        projectJP_csv.append(lineCSV)


class ProjectSF:
    project_sf = pd.read_excel(url + 'project_SF.xlsx', engine='openpyxl')
    lines = list(project_sf.T.to_dict().values())
    id_cargo_type = str
    tag = str
    for line in lines:

        tag = 'client:|code:' + line['Code']

        if line['Type'] == 'Container':
            id_cargo_type = 'teu'
        else:
            id_cargo_type = 'conventional'

        lineCSV = {
            'PORT_ORIGIN': line['POL'],
            'PORT_DESTINATION': line['POD'],
            'STAGE_DISTANCE': "",
            'VEHICLES_NUMBER': "1",
            'ID_CARGO_TYPE': id_cargo_type,
            'TAGS': tag,
            'MATERIAL_NAME': "",
            'CARGO_TEU': line['uds de transporte'],
            'CARGO_WEIGHT': line['Mt/ud'],
            'GROSS_TONNAGE': "",
            'TOTAL_LOAD': line['Mt'],
            'ID_SHIP_LOAD_TYPE': line['Type'],
            'ID_SHIP_TYPE': "",
            'PORT_OPERATIONS': "",
            'SHIP_FUEL_TYPE': ""
        }
        lines_csv.append(lineCSV)
        projectSF_csv.append(lineCSV)


class ProjectPE:
    project_pe = pd.read_excel(url + 'project_PE.xlsx', engine='openpyxl')
    lines = list(project_pe.T.to_dict().values())
    id_cargo_type = str
    tag = str
    for line in lines:
        tag = 'client:' + line['Owr'] + '|code:' + line['Code']

        if line['Type'] == 'Container':
            id_cargo_type = 'teu'
        else:
            id_cargo_type = 'conventional'
        lineCSV = {
            'PORT_ORIGIN': line['POL'],
            'PORT_DESTINATION': line['POD'],
            'STAGE_DISTANCE': "",
            'VEHICLES_NUMBER': "1",
            'ID_CARGO_TYPE': id_cargo_type,
            'TAGS': tag,
            'MATERIAL_NAME': "",
            'CARGO_TEU': line['uds de transporte'],
            'CARGO_WEIGHT': line['MT/ud'],
            'GROSS_TONNAGE': "",
            'TOTAL_LOAD': line['Mt'],
            'ID_SHIP_LOAD_TYPE': line['Type'],
            'ID_SHIP_TYPE': "",
            'PORT_OPERATIONS': "",
            'SHIP_FUEL_TYPE': ""
        }
        lines_csv.append(lineCSV)
        projectPE_csv.append(lineCSV)


class ProjectJM:
    project_jm = pd.read_excel(url + 'project_JM.xlsx', engine='openpyxl')
    lines = list(project_jm.T.to_dict().values())
    id_cargo_type = str
    tag = str
    for line in lines:
        tag = 'client:' + line['Owr'] + '|code:' + line['Code']
        if line['Type'] == 'Container':
            id_cargo_type = 'teu'
        else:
            id_cargo_type = 'conventional'

        lineCSV = {
            'PORT_ORIGIN': line['POL'],
            'PORT_DESTINATION': line['POD'],
            'STAGE_DISTANCE': "",
            'VEHICLES_NUMBER': "1",
            'ID_CARGO_TYPE': id_cargo_type,
            'TAGS': tag,
            'MATERIAL_NAME': "",
            'CARGO_TEU': line['uds de transporte'],
            'CARGO_WEIGHT': line['Mt/ud'],
            'GROSS_TONNAGE': "",
            'TOTAL_LOAD': line['Total MT'],
            'ID_SHIP_LOAD_TYPE': line['Type'],
            'ID_SHIP_TYPE': "",
            'PORT_OPERATIONS': "",
            'SHIP_FUEL_TYPE': ""
        }
        lines_csv.append(lineCSV)
        projectJM_csv.append(lineCSV)


class CarbonMark2022:
    carbon_mark_2022 = pd.read_excel(url + 'Huella_de_Carbono_2022.xlsx', engine='openpyxl')
    carbon_mark_2022 = carbon_mark_2022.dropna(how='all')
    lines = list(carbon_mark_2022.T.to_dict().values())
    for line in lines:
        lineCSV = {
            'PORT_ORIGIN': line['Origen'],
            'PORT_DESTINATION': line['Destino'],
            'STAGE_DISTANCE': "",
            'VEHICLES_NUMBER': line['N camiones'],
            'ID_CARGO_TYPE': "id_cargo_type",
            'TAGS': line["N Proyecto K2"],
            'MATERIAL_NAME': line['Mercancia'],
            'CARGO_TEU': "",
            'CARGO_WEIGHT': "",
            'GROSS_TONNAGE': "",
            'TOTAL_LOAD': "",
            'ID_SHIP_LOAD_TYPE': "",
            'ID_SHIP_TYPE': "",
            'PORT_OPERATIONS': "",
            'SHIP_FUEL_TYPE': ""
        }
        lines_csv.append(lineCSV)
        huella_de_carbono_2022_csv.append(lineCSV)


class Mark2022:
    mark_2022 = pd.read_excel(url + 'HUELLA_2022.xlsx', engine='openpyxl', sheet_name='HUELLA')
    lines = list(mark_2022.T.to_dict().values())
    for line in lines:
        lineCSV = {
            'PORT_ORIGIN': line['ORIGEN'],
            'PORT_DESTINATION': line['DESTINO'],
            'STAGE_DISTANCE': "",
            'VEHICLES_NUMBER': '1',
            'ID_CARGO_TYPE': '',
            'TAGS': '',
            'MATERIAL_NAME': '',
            'CARGO_TEU': line['Nº CONTENEDORES'],
            'CARGO_WEIGHT': "",
            'GROSS_TONNAGE': "",
            'TOTAL_LOAD': line['KILOS BRUTOS'],
            'ID_SHIP_LOAD_TYPE': "",
            'ID_SHIP_TYPE': "",
            'PORT_OPERATIONS': "",
            'SHIP_FUEL_TYPE': ''
        }
        lines_csv.append(lineCSV)
        HUELLA_2022_csv.append(lineCSV)


class Locode:
    ports = []
    for line in lines_csv:
        addPort = True
        i = 0
        if len(ports) == 0:
            ports.append(line["PORT_DESTINATION"])
        while i < len(ports):
            if line["PORT_DESTINATION"] == ports[i]:
                addPort = False
            i = i + 1
        if addPort:
            if line["PORT_DESTINATION"] != "nan":
                ports.append(line["PORT_DESTINATION"])

    for line in lines_csv:
        print(line['PORT_ORIGIN'], ' | ', line['PORT_DESTINATION'])

        if line["PORT_ORIGIN"] == "Vigo":
            line["PORT_ORIGIN"] = "ESVGO"

        if line["PORT_ORIGIN"] == "VIGO":
            line["PORT_ORIGIN"] = "ESVGO"

        if line["PORT_ORIGIN"] == "Kaohsiung":
            line["PORT_ORIGIN"] = "TWKHH"

        if line["PORT_ORIGIN"] == "Namibe":
            line["PORT_ORIGIN"] = "AOMSZ"

        if line["PORT_ORIGIN"] == "Leixoes":
            line["PORT_ORIGIN"] = "IPLEI"

        if line["PORT_ORIGIN"] == "LEIXOES":
            line["PORT_ORIGIN"] = "IPLEI"

        if line["PORT_ORIGIN"] == "Larvik ":
            line["PORT_ORIGIN"] = "SINLAR"

        if line["PORT_ORIGIN"] == "Beira":
            line["PORT_ORIGIN"] = "MZ BEW"

        if line["PORT_ORIGIN"] == "Villagarcia":
            line["PORT_ORIGIN"] = "ES VIL"

        if line["PORT_ORIGIN"] == "Salvador":
            line["PORT_ORIGIN"] = "BR SSA"

        if line["PORT_ORIGIN"] == "Vitoria":
            line["PORT_ORIGIN"] = "BR VIX"

        if line["PORT_ORIGIN"] == "Richars Bay":
            line["PORT_ORIGIN"] = "ZARCB"

        if line["PORT_ORIGIN"] == "nan":
            line["PORT_ORIGIN"] = ""

        if line["PORT_ORIGIN"] == "Durban":
            line["PORT_ORIGIN"] = "ZADUR"

        if line["PORT_ORIGIN"] == "Cape Town":
            line["PORT_ORIGIN"] = "ZACPT"

        if line["PORT_ORIGIN"] == "MDC":
            line["PORT_ORIGIN"] = "ES MDC"

        if line["PORT_ORIGIN"] == "Wuzhou":
            line["PORT_ORIGIN"] = "CNWUZ"

        if line["PORT_ORIGIN"] == "Johanesburg":
            line["PORT_ORIGIN"] = "ZAHDL"

        if line["PORT_ORIGIN"] == "Luanda":
            line["PORT_ORIGIN"] = "AOLAD"

        if line["PORT_ORIGIN"] == "Moimenta da Beira (PT)":
            line["PORT_ORIGIN"] = "PTMBR"

        if line["PORT_ORIGIN"] == "Aveiro":
            line["PORT_ORIGIN"] = "PTAVE"

        if line["PORT_ORIGIN"] == "Skikda":
            line["PORT_ORIGIN"] = "DZSKK"

        if line["PORT_DESTINATION"] == "Namibe":
            line["PORT_DESTINATION"] = "AOMSZ"

        if line["PORT_DESTINATION"] == "Xiamen":
            line["PORT_DESTINATION"] = "CNXMN"

        if line["PORT_DESTINATION"] == "La Spezia":
            line["PORT_DESTINATION"] = "ITSPL"

        if line["PORT_DESTINATION"] == "Szczecin":
            line["PORT_DESTINATION"] = "PLSZZ"

        if line["PORT_DESTINATION"] == "Vigo":
            line["PORT_DESTINATION"] = "ESVGO"

        if line["PORT_DESTINATION"] == "Antwerp":
            line["PORT_DESTINATION"] = "BEANR"

        if line["PORT_DESTINATION"] == "Bremerhaven":
            line["PORT_DESTINATION"] = "DEBRV"

        if line["PORT_DESTINATION"] == "London":
            line["PORT_DESTINATION"] = "GBLON"

        if line["PORT_DESTINATION"] == "Pozzallo":
            line["PORT_DESTINATION"] = "ITPOZ"

        if line["PORT_DESTINATION"] == "Hualien":
            line["PORT_DESTINATION"] = "TWHUN"

        if line["PORT_DESTINATION"] == "Sousse":
            line["PORT_DESTINATION"] = "TNSFA"

        if line["PORT_DESTINATION"] == "Tartous":
            line["PORT_DESTINATION"] = "SYTTS"

        if line["PORT_DESTINATION"] == "Le Havre":
            line["PORT_DESTINATION"] = "FRLEH"

        if line["PORT_DESTINATION"] == "Maputo":
            line["PORT_DESTINATION"] = "MZMPM"

        if line["PORT_DESTINATION"] == "Mundra":
            line["PORT_DESTINATION"] = "INMUN"

        if line["PORT_DESTINATION"] == "Keelung":
            line["PORT_DESTINATION"] = "TWKEL"

        if line["PORT_DESTINATION"] == "Kaohsiung":
            line["PORT_DESTINATION"] = "TWKHH"

        if line["PORT_DESTINATION"] == "Jebel Ali":
            line["PORT_DESTINATION"] = "AEJEA"

        if line["PORT_DESTINATION"] == "Harare":
            line["PORT_DESTINATION"] = "ZWHRE"

        if line["PORT_DESTINATION"] == "Gdynia":
            line["PORT_DESTINATION"] = "PLGDY"

        if line["PORT_DESTINATION"] == "Ho Chi Minh":
            line["PORT_DESTINATION"] = "VNSGN"

        if line["PORT_DESTINATION"] == "Durres":
            line["PORT_DESTINATION"] = "ALDUR"

        if line["PORT_DESTINATION"] == "Qingdao":
            line["PORT_DESTINATION"] = "CNTAO"

        if line["PORT_DESTINATION"] == "Azuqueca de Henares (Guadalajara)":
            line["PORT_DESTINATION"] = "ESAZJ"

        if line["PORT_DESTINATION"] == "Irurtzun (Navarra)":
            line["PORT_DESTINATION"] = "ESIRT"

        if line["PORT_DESTINATION"] == "Orcoyen (Navarra)":
            line["PORT_DESTINATION"] = "ESORO"

        if line["PORT_DESTINATION"] == "Mondragón (Guipúzcoa)":
            line["PORT_DESTINATION"] = "ESMDR"

        if line["PORT_DESTINATION"] == "Arganda del Rey (Madrid)":
            line["PORT_DESTINATION"] = "ESAGR"

        if line["PORT_DESTINATION"] == "Padrón":
            line["PORT_DESTINATION"] = "ESPDR"

        if line["PORT_DESTINATION"] == "San Marino (Italia)":
            line["PORT_DESTINATION"] = "SMSMT"

        if line["PORT_DESTINATION"] == "Valga (Pontevedra)":
            line["PORT_DESTINATION"] = "ESVAL"

        if line["PORT_DESTINATION"] == "Abadiño (Vizcaya)":
            line["PORT_DESTINATION"] = "ESABD"

        if line["PORT_DESTINATION"] == "Aldeanueva de Ebro (La Rioja)":
            line["PORT_DESTINATION"] = "ESADE"

        if line["PORT_DESTINATION"] == "O Porriño":
            line["PORT_DESTINATION"] = "ESPOR"

        if line["PORT_DESTINATION"] == "REDONDELA":
            line["PORT_DESTINATION"] = "ESRDA"

        if line["PORT_DESTINATION"] == "POBRA DO CARAMIÑAL":
            line["PORT_DESTINATION"] = "ESPBC"

        if line["PORT_DESTINATION"] == "Shihu":
            line["PORT_DESTINATION"] = "CNSHU"

        if line["PORT_DESTINATION"] == "Thailand":
            line["PORT_DESTINATION"] = "Thailand"

        if line["PORT_ORIGIN"] == "Aveiro":
            line["PORT_ORIGIN"] = "ESCOCHE"

        if line["PORT_ORIGIN"] == "Cartagena":
            line["PORT_ORIGIN"] = "COCTG"

    for port in ports:
        print(port)


class OrigenDestino:
    for line in lines_csv:

        if line['PORT_ORIGIN'] == 'TWKHH':
            if line['PORT_DESTINATION'] == 'AOMSZ':
                line['STAGE_DISTANCE'] = '-'

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'CNXMN':
                line['STAGE_DISTANCE'] = 15597790

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'ITSPL':
                line['STAGE_DISTANCE'] = 9043030

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'PLSZZ':
                line['STAGE_DISTANCE'] = 2943700

        if line['PORT_ORIGIN'] == 'IPLEI':
            if line['PORT_DESTINATION'] == 'AOMSZ':
                line['STAGE_DISTANCE'] = 7788250

        if line['PORT_ORIGIN'] == 'SINLAR':
            if line['PORT_DESTINATION'] == 'ESVGO':
                line['STAGE_DISTANCE'] = 2359990

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'ESVGO':
                line['STAGE_DISTANCE'] = 7896330

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'ESVGO':
                line['STAGE_DISTANCE'] = 7896330

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'MDC':
                line['STAGE_DISTANCE'] = 1045310

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'MDC':
                line['STAGE_DISTANCE'] = 8900920

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'PLSZZ':
                line['STAGE_DISTANCE'] = 2834220

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'ESAZJ':
                line['STAGE_DISTANCE'] = 2834220

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'ESPDR':
                line['STAGE_DISTANCE'] = 70390

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'ESPBC':
                line['STAGE_DISTANCE'] = 65180

        if line['PORT_ORIGIN'] == 'ES MDC':
            if line['PORT_DESTINATION'] == 'ESVGO':
                line['STAGE_DISTANCE'] = 1045310

        if line['PORT_ORIGIN'] == 'MZ BEW':
            if line['PORT_DESTINATION'] == 'MDC':
                line['STAGE_DISTANCE'] = 0

        if line['PORT_ORIGIN'] == 'MZ BEW':
            if line['PORT_DESTINATION'] == 'ESVGO':
                line['STAGE_DISTANCE'] = 11759940

        if line['PORT_ORIGIN'] == 'BR SSA':
            if line['PORT_DESTINATION'] == 'BEANR':
                line['STAGE_DISTANCE'] = 8391030

        if line['PORT_ORIGIN'] == 'BR VIX':
            if line['PORT_DESTINATION'] == 'DEBRV':
                line['STAGE_DISTANCE'] = 9569370

        if line['PORT_ORIGIN'] == 'ZARCB':
            if line['PORT_DESTINATION'] == 'ESVGO':
                line['STAGE_DISTANCE'] = 11408140

        if line['PORT_ORIGIN'] == 'BR VIX':
            if line['PORT_DESTINATION'] == 'GBLON':
                line['STAGE_DISTANCE'] = 9118430

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'ITPOZ':
                line['STAGE_DISTANCE'] = 2752210

        if line['PORT_ORIGIN'] == 'ZADUR':
            if line['PORT_DESTINATION'] == 'ESVGO':
                line['STAGE_DISTANCE'] = 11296880

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'TWHUN':
                line['STAGE_DISTANCE'] = 15531180

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'TNSFA':
                line['STAGE_DISTANCE'] = 2559740

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'SYTTS':
                line['STAGE_DISTANCE'] = 4840410

        if line['PORT_ORIGIN'] == 'MZ BEW':
            if line['PORT_DESTINATION'] == 'MDC':
                line['STAGE_DISTANCE'] = 12672380

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'TWKEL':
                line['STAGE_DISTANCE'] = 15599440

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'INMUN':
                line['STAGE_DISTANCE'] = 5859920

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'AEJEA':
                line['STAGE_DISTANCE'] = 11001790

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'TWKHH':
                line['STAGE_DISTANCE'] = 15249570

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'ESORO':
                line['STAGE_DISTANCE'] = 830800

        if line['PORT_ORIGIN'] == 'IPLEI':
            if line['PORT_DESTINATION'] == 'ESABD':
                line['STAGE_DISTANCE'] = 131570

        if line['PORT_ORIGIN'] == 'IPLEI':
            if line['PORT_DESTINATION'] == 'ESABD':
                line['STAGE_DISTANCE'] = 131570

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'BEANR':
                line['STAGE_DISTANCE'] = 9406320

        if line['PORT_ORIGIN'] == 'ZADUR':
            if line['PORT_DESTINATION'] == 'FRLEH':
                line['STAGE_DISTANCE'] = 12407290

        if line['PORT_ORIGIN'] == 'CNWUZ':
            if line['PORT_DESTINATION'] == 'ZWHRE':
                line['STAGE_DISTANCE'] = 11520450

        if line['PORT_ORIGIN'] == 'IPLEI':
            if line['PORT_DESTINATION'] == 'MZMPM':
                line['STAGE_DISTANCE'] = 11652000

        if line['PORT_ORIGIN'] == 'IPLEI':
            if line['PORT_DESTINATION'] == 'ESRDA':
                line['STAGE_DISTANCE'] = 131570

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'ESRDA':
                line['STAGE_DISTANCE'] = '-'

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'ESVAL':
                line['STAGE_DISTANCE'] = 3199150

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'ESMDR':
                line['STAGE_DISTANCE'] = 701590

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'SMSMT':
                line['STAGE_DISTANCE'] = 2113560

        if line['PORT_ORIGIN'] == 'ES VIL':
            if line['PORT_DESTINATION'] == 'SMSMT':
                line['STAGE_DISTANCE'] = 3903290

        if line['PORT_ORIGIN'] == 'ESVGO':
            if line['PORT_DESTINATION'] == 'ESAGR':
                line['STAGE_DISTANCE'] = '-'

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'CNSHU ':
                line['STAGE_DISTANCE'] = '-'

        if line['PORT_ORIGIN'] == 'MZ BEW':
            if line['PORT_DESTINATION'] == 'PLGDY':
                line['STAGE_DISTANCE'] = 14709420

        if line['PORT_ORIGIN'] == 'MZ BEW':
            if line['PORT_DESTINATION'] == 'ITSPL':
                line['STAGE_DISTANCE'] = 9832130

        if line['PORT_ORIGIN'] == 'PTMBR':
            if line['PORT_DESTINATION'] == 'ESVGO':
                line['STAGE_DISTANCE'] = 354150

        if line['PORT_ORIGIN'] == 'ES VIL':
            if line['PORT_DESTINATION'] == 'ESVGO':
                line['STAGE_DISTANCE'] = 44880

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'FRLEH':
                line['STAGE_DISTANCE'] = 9058920

        if line['PORT_ORIGIN'] == 'AOLAD':
            if line['PORT_DESTINATION'] == 'CNXMN':
                line['STAGE_DISTANCE'] = 16299880

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'VNSGN':
                line['STAGE_DISTANCE'] = 13711270

        if line['PORT_ORIGIN'] == 'AOMSZ':
            if line['PORT_DESTINATION'] == 'PLGDY':
                line['STAGE_DISTANCE'] = 10826180

        if line['PORT_ORIGIN'] == 'PTAVE':
            if line['PORT_DESTINATION'] == 'DZSKK':
                line['STAGE_DISTANCE'] = 0

        if line['PORT_ORIGIN'] == 'COCTG':
            if line['PORT_DESTINATION'] == 'MZMPM':
                line['STAGE_DISTANCE'] = 13361390

        if line['PORT_ORIGIN'] == '':
            if line['PORT_DESTINATION'] == '':
                line['STAGE_DISTANCE'] = 0

        if line['STAGE_DISTANCE'] == '':
            line['STAGE_DISTANCE'] = 0


class Transport_Method:
    tipos = []
    for line in lines_csv:
        addType = True
        i = 0
        if len(tipos) == 0:
            tipos.append(line)
        while i < len(tipos):
            if line["ID_SHIP_LOAD_TYPE"] == tipos[i]:
                addType = False
            i = i + 1
        if addType:
            tipos.append(line["ID_SHIP_LOAD_TYPE"])

        if line["ID_SHIP_LOAD_TYPE"] == "Container":
            ship_type.append(line)

        if line["ID_SHIP_LOAD_TYPE"] == "RORO":
            ship_type.append(line)

        if line["ID_SHIP_LOAD_TYPE"] == "Inland truck":
            ship_type.append(line)

        if line["ID_SHIP_LOAD_TYPE"] == "Others":
            ship_type.append(line)

        if line["ID_SHIP_LOAD_TYPE"] == "Rail":
            railLine = {
                'PORT_ORIGIN': line['PORT_ORIGIN'],
                'PORT_DESTINATION': line['PORT_DESTINATION'],
                'STAGE_DISTANCE': line['STAGE_DISTANCE'],
                'VEHICLES_NUMBER': "1",
                'ID_CARGO_TYPE': line['ID_CARGO_TYPE'],
                'TAGS': line['TAGS'],
                'MATERIAL_NAME': line['MATERIAL_NAME'],
                'CARGO_TEU': line['CARGO_TEU'],
                'CARGO_WEIGHT': line['CARGO_WEIGHT'],
                'GROSS_TONNAGE': line['GROSS_TONNAGE'],
                'TOTAL_LOAD': line['TOTAL_LOAD'],
                'ID_TRAIN_LOAD_TYPE': line['ID_SHIP_LOAD_TYPE'],
                'ID_TRAIN_TYPE': line['ID_SHIP_TYPE'],
                'PORT_OPERATIONS': line['PORT_OPERATIONS'],
                'TRAIN_FUEL_TYPE': line['SHIP_FUEL_TYPE']
            }
            rail_type.append(railLine)

        if line["ID_SHIP_LOAD_TYPE"] == "Break Bulk":
            break_bulk_type.append(line)


class RailBulk:
    with open(rail_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=rail_type[0].keys())
        writer.writeheader()
        for data in rail_type:
            writer.writerow(data)


class BreakBulkCSV:
    with open(break_bulk_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=break_bulk_type[0].keys())
        writer.writeheader()
        for data in break_bulk_type:
            writer.writerow(data)


class todoCSV:
    with open(todo, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=lines_csv[0].keys())
        writer.writeheader()
        for data in lines_csv:
            writer.writerow(data)


class ShipCSV:
    with open(ship_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=ship_type[0].keys())
        writer.writeheader()
        for data in ship_type:
            writer.writerow(data)


class projectJPCsv:
    with open(projectJP, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=projectJP_csv[0].keys())
        writer.writeheader()
        for data in projectJP_csv:
            writer.writerow(data)


class projectSFCsv:
    with open(projectSF, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=projectSF_csv[0].keys())
        writer.writeheader()
        for data in projectSF_csv:
            writer.writerow(data)


class projectPECsv:
    with open(projectPE, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=projectPE_csv[0].keys())
        writer.writeheader()
        for data in projectPE_csv:
            writer.writerow(data)


class projectJMCsv:
    with open(projectJM, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=projectJM_csv[0].keys())
        writer.writeheader()
        for data in projectJM_csv:
            writer.writerow(data)


class huella2022CSV:
    with open(HUELLA_2022, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=HUELLA_2022_csv[0].keys())
        writer.writeheader()
        for data in HUELLA_2022_csv:
            writer.writerow(data)


class hueellaDeCarbono2022CSV:
    with open(huella_de_carbono_2022, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=huella_de_carbono_2022_csv[0].keys())
        writer.writeheader()
        for data in huella_de_carbono_2022_csv:
            writer.writerow(data)
